﻿namespace BST.App
{
    public interface INode<T>
    {
        /// <summary>
        /// Gets node value.
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Gets or sets parent node.
        /// </summary>
        INode<T> Parent { get; set; }

        /// <summary>
        /// Gets or sets left child (smaller value)
        /// </summary>
        INode<T> LeftChild { get; set; }

        /// <summary>
        /// Gets or sets right child (bigger value)
        /// </summary>
        INode<T> RightChild { get; set; }
    }
}
