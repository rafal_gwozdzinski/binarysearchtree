﻿using System;

namespace BST.App
{
    class Program
    {
        static IBinarySearchTree<int> tree = new BinarySearchTree<int>();

        /// <summary>
        /// Main method.
        /// </summary>
        static void Main()
        {
            while (true)
            {
                if (ProcessLine() == false)
                {
                    return;
                }
            }
        }

        private static bool ProcessLine()
        {
            string[] line = Console.ReadLine().Split();

            switch (line[0])
            {
                case "i":
                    Insert(line[1]);
                    break;
                case "d":
                    Delete(line[1]);
                    break;
                case "h":
                    PrintHeight();
                    break;
                default:
                    return false;
            }

            return true;
        }

        private static void Insert(string value)
        {
            try
            {
                tree.Add(int.Parse(value));
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong value.");
            }
        }

        private static void PrintHeight()
        {
            Console.WriteLine(tree.Height);
        }

        private static void Delete(string value)
        {
            try
            {
                if (tree.Remove(int.Parse(value)) == false)
                {
                    Console.WriteLine("Could not remove value.");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong value.");
            }
        }
    }
}
