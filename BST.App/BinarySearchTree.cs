﻿using System;
using System.Collections.Generic;

namespace BST.App
{
    public class BinarySearchTree<T> : IBinarySearchTree<T>
    {

        #region Constructors

        /// <summary>
        /// The constructor.
        /// </summary>
        public BinarySearchTree()
        {
            Comparer = Comparer<T>.Default;
        }

        #endregion

        #region Operations

        /// <summary>
        /// Adds value to the tree.
        /// </summary>
        /// <param name="value">Value to add</param>
        public void Add(T value)
        {
            INode<T> node = new Node<T>(value);

            if (Root == null)
            {
                Root = node;
                return;
            }

            INode<T> current = Root;
            INode<T> previous = null;
            int compareResult;

            while (current != null)
            {
                compareResult = Comparer.Compare(value, current.Value);
                if (compareResult == 0)
                {
                    return;
                }
                else if (compareResult > 0)
                {
                    previous = current;
                    current = current.RightChild;
                }
                else
                {
                    previous = current;
                    current = current.LeftChild;
                }
            }

            compareResult = Comparer.Compare(previous.Value, value);
            if (compareResult > 0)
            {
                previous.LeftChild = node;
            }
            else
            {
                previous.RightChild = node;
            }
            node.Parent = previous;
        }

        /// <summary>
        /// Adds multiple values to the tree.
        /// </summary>
        /// <param name="values">Values to add</param>
        public void Add(IEnumerable<T> values)
        {
            foreach (T value in values)
            {
                Add(value);
            }
        }

        private int FindHeight(INode<T> node)
        {
            if (node == null) return 0;

            int leftHeight = FindHeight(node.LeftChild);
            int rightHeight = FindHeight(node.RightChild);

            return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
        }

        /// <summary>
        /// Removes value from the tree.
        /// </summary>
        /// <param name="value">Value to remove</param>
        /// <returns>Returns true if removed, false otherwise</returns>
        public bool Remove(T value)
        {
            INode<T> removedNode = SearchNode(value);

            if (removedNode == null)
            {
                return false;
            }

            if (removedNode.RightChild == null)
            {
                RemoveNodeWithNoRightChild(removedNode);
            }
            else if (removedNode.RightChild.LeftChild == null)
            {
                RemoveNodeWithRightChildThatHasNoLeftChild(removedNode);
            }
            else
            {
                RemoveNodeWithRightChildThatHasLeftChild(removedNode);
            }

            return true;
        }

        private void RemoveNodeWithNoRightChild(INode<T> node)
        {
            if (node.Parent == null)
            {
                Root = node.LeftChild;
                Root.Parent = null;
            }
            else
            {
                int compareResult = Comparer.Compare(node.Parent.Value, node.Value);
                if (compareResult > 0)
                {
                    node.Parent.LeftChild = node.LeftChild;
                }
                else
                {
                    node.Parent.RightChild = node.LeftChild;
                }
                if (node.LeftChild != null)
                {
                    node.LeftChild.Parent = node.Parent;
                }                
            }

        }

        private void RemoveNodeWithRightChildThatHasNoLeftChild(INode<T> node)
        {
            node.RightChild.LeftChild = node.LeftChild;

            if (node.Parent == null)
            {
                Root = node.RightChild;
                Root.Parent = null;
            }
            else
            {
                int compareResult = Comparer.Compare(node.Parent.Value, node.Value);
                if (compareResult > 0)
                {
                    node.Parent.LeftChild = node.RightChild;
                }
                else
                {
                    node.Parent.RightChild = node.RightChild;
                }
                if (node.RightChild != null)
                {
                    node.RightChild.Parent = node.Parent;
                }
            }            
        }

        private void RemoveNodeWithRightChildThatHasLeftChild(INode<T> node)
        {
            INode<T> leftmostChildNode = FindLeftmostChildNode(node.RightChild);
            leftmostChildNode.Parent.LeftChild = leftmostChildNode.RightChild;

            leftmostChildNode.LeftChild = node.LeftChild;
            leftmostChildNode.RightChild = node.RightChild;

            if (node.Parent == null)
            {
                Root = leftmostChildNode;
                Root.Parent = null;
            }
            else
            {
                int compareResult = Comparer.Compare(node.Parent.Value, node.Value);
                if (compareResult > 0)
                {
                    node.Parent.LeftChild = leftmostChildNode;
                }
                else
                {
                    node.Parent.RightChild = leftmostChildNode;
                }
                leftmostChildNode.Parent = node.Parent;
            }
        }

        private INode<T> FindLeftmostChildNode(INode<T> node)
        {
            while (node.LeftChild != null)
            {
                node = node.LeftChild;
            }
            return node;
        }

        public INode<T> SearchNode(T value)
        {
            if (Root == null)
            {
                return null;
            }

            INode<T> current = Root;
            INode<T> previous = null;
            int compareResult = Comparer.Compare(current.Value, value);
            while (compareResult != 0)
            {
                if (compareResult > 0)
                {
                    previous = current;
                    current = current.LeftChild;
                }
                else if (compareResult < 0)
                {
                    previous = current;
                    current = current.RightChild;
                }

                if (current == null)
                {
                    return null;
                }
                else
                {
                    compareResult = Comparer.Compare(current.Value, value);
                }
            }

            return current;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets comparer used by tree.
        /// </summary>
        public Comparer<T> Comparer { get; set; }

        /// <summary>
        /// Gets height of the tree.
        /// </summary>
        public int Height
        {
            get { return FindHeight(Root); }
        }

        /// <summary>
        /// Gets root node of the tree.
        /// </summary>
        public INode<T> Root { get; set; }

        #endregion

    }
}
