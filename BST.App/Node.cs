﻿namespace BST.App
{
    public class Node<T> : INode<T>
    {
        /// <summary>
        /// The constructor.
        /// </summary>
        /// <param name="value">Node value</param>
        public Node(T value)
        {
            Value = value;
        }

        /// <summary>
        /// Gets or sets parent node.
        /// </summary>
        public INode<T> Parent { get; set; }

        /// <summary>
        /// Gets or sets left child (smaller value)
        /// </summary>
        public INode<T> LeftChild { get; set; }

        /// <summary>
        /// Gets or sets right child (bigger value)
        /// </summary>
        public INode<T> RightChild { get; set; }

        /// <summary>
        /// Gets node value.
        /// </summary>
        public T Value { get; private set; }
    }
}
