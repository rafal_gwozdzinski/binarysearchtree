﻿using System.Collections.Generic;

namespace BST.App
{
    public interface IBinarySearchTree<T>
    {
        /// <summary>
        /// Adds value to the tree.
        /// </summary>
        /// <param name="value">Value to add</param>
        void Add(T value);

        /// <summary>
        /// Adds multiple values to the tree.
        /// </summary>
        /// <param name="values">Values to add</param>
        void Add(IEnumerable<T> values);

        /// <summary>
        /// Removes value from the tree.
        /// </summary>
        /// <param name="value">Value to remove</param>
        /// <returns>Returns true if removed, false otherwise</returns>
        bool Remove(T value);

        /// <summary>
        /// Gets or sets comparer used by tree.
        /// </summary>
        Comparer<T> Comparer { get; set; }

        /// <summary>
        /// Gets height of the tree.
        /// </summary>
        int Height { get; }

        /// <summary>
        /// Gets root node of the tree.
        /// </summary>
        INode<T> Root { get; set; }
    }
}
