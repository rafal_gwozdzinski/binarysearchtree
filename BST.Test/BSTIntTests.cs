﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BST.App;

namespace BST.Test
{
    [TestClass]
    public class BSTIntTests
    {
        [TestMethod]
        public void AddRoot()
        {
            // Arrange
            int rootValue = 42;
            IBinarySearchTree<int> tree = new BinarySearchTree<int>();

            // Act
            tree.Add(rootValue);

            // Assert
            Assert.AreEqual(rootValue, tree.Root.Value);
        }

        [TestMethod]
        public void AddThree()
        {
            // Arrange
            int rootValue = 42;
            int firstValue = 41;
            int secondValue = 43;
            IBinarySearchTree<int> tree = new BinarySearchTree<int>();

            // Act
            tree.Add(new int[] { rootValue, firstValue, secondValue });

            // Assert
            Assert.AreEqual(rootValue, tree.Root.Value);
            Assert.AreEqual(firstValue, tree.Root.LeftChild.Value);
            Assert.AreEqual(secondValue, tree.Root.RightChild.Value);
        }

        [TestMethod]
        public void HeightOfEmpty()
        {
            // Arrange
            IBinarySearchTree<int> tree = new BinarySearchTree<int>();

            // Act
            int height = tree.Height;

            // Assert
            Assert.AreEqual(0, height);
        }

        [TestMethod]
        public void HeightOfNotEmpty()
        {
            // Arrange
            int rootValue = 42;
            int firstValue = 41;
            int secondValue = 40;
            int thirdValue = 39;
            IBinarySearchTree<int> tree = new BinarySearchTree<int>();
            tree.Add(new int[] { rootValue, firstValue, secondValue, thirdValue });

            // Act
            int height = tree.Height;

            // Assert
            Assert.AreEqual(4, tree.Height);
        }

        [TestMethod]
        public void SearchEmpty()
        {
            // Arrange
            BinarySearchTree<int> tree = new BinarySearchTree<int>();

            // Act
            INode<int> node = tree.SearchNode(12);

            // Assert
            Assert.IsNull(node);
        }

        [TestMethod]
        public void SearchPopulated()
        {
            // Arrange
            int rootValue = 42;
            int firstValue = 41;
            int secondValue = 40;
            int thirdValue = 39;
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            tree.Add(new int[] { rootValue, firstValue, secondValue, thirdValue });

            // Act
            INode<int> node = tree.SearchNode(thirdValue);

            // Assert
            Assert.AreEqual(thirdValue, node.Value);
        }

        [TestMethod]
        public void RemoveCase1()
        {
            // Arrange
            int[] values = { 90, 50, 150, 20, 5, 25 };
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            tree.Add(values);

            // Act
            tree.Remove(50);

            // Assert
            Assert.IsNull(tree.SearchNode(50));
            Assert.AreEqual(90, tree.SearchNode(20).Parent.Value);
            Assert.AreEqual(3, tree.Height);
        }

        [TestMethod]
        public void RemoveCase2()
        {
            // Arrange
            int[] values = { 90, 50, 150, 20, 125, 175, 5, 25, 140 };
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            tree.Add(values);

            // Act
            tree.Remove(150);

            // Assert
            Assert.IsNull(tree.SearchNode(150));
            Assert.AreEqual(90, tree.SearchNode(175).Parent.Value);
            Assert.AreEqual(4, tree.Height);
        }

        [TestMethod]
        public void RemoveCase3()
        {
            // Arrange
            int[] values = { 90, 50, 150, 20, 75, 5, 66, 80, 68 };
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            tree.Add(values);

            // Act
            tree.Remove(50);

            // Assert
            Assert.IsNull(tree.SearchNode(50));
            Assert.AreEqual(90, tree.SearchNode(66).Parent.Value);
            Assert.AreEqual(4, tree.Height);
        }
    }
}
