﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BST.App;

namespace BST.Test
{
    [TestClass]
    public class Examples
    {
        [TestMethod]
        public void Example1()
        {
            IBinarySearchTree<int> tree = new BinarySearchTree<int>();

            tree.Add(10);
            Assert.AreEqual(1, tree.Height);

            tree.Add(15);
            tree.Add(12);
            Assert.AreEqual(3, tree.Height);

            tree.Remove(12);
            tree.Add(17);
            tree.Add(16);
            tree.Add(2);
            tree.Remove(16);
            Assert.AreEqual(3, tree.Height);
        }

        [TestMethod]
        public void Example2()
        {
            IBinarySearchTree<int> tree = new BinarySearchTree<int>();

            tree.Add(10);
            Assert.AreEqual(1, tree.Height);

            tree.Add(5);
            Assert.AreEqual(2, tree.Height);

            tree.Add(1);
            Assert.AreEqual(3, tree.Height);

            tree.Add(6);
            Assert.AreEqual(3, tree.Height);

            tree.Remove(10);
            Assert.AreEqual(2, tree.Height);

            tree.Remove(5);
            Assert.AreEqual(2, tree.Height);
        }
    }
}
